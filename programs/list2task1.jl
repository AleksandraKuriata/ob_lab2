#Aleksandra Kuriata

Float32x = Float32[2.718281828, -3.141592654, 1.414213562, 0.577215664, 0.301029995]
Float32y = Float32[1486.2497, 878366.9879, -22.37492, 4773714.647, 0.000185049]

Float64x = Float64[2.718281828, -3.141592654, 1.414213562, 0.577215664, 0.301029995]
Float64y = Float64[1486.2497, 878366.9879, -22.37492, 4773714.647, 0.000185049]

#(a)
function frontFloat32()
  S = Float32(0.0)
  for i = 1 : 5
    S = S + Float32x[i] * Float32y[i]
  end
  println("front Float32: $(S)")
  return S
end
frontFloat32()

function frontFloat64()
  S = Float64(0.0)
  for i = 1 : 5
    S = S + Float64x[i] * Float64y[i]
  end
    println("front Float64: $(S)")
  return S
end
frontFloat64()


#(b)
function backFloat32()
  S = Float32(0.0)
  for i = 5 : -1 : 1
      S = S + Float32x[i] * Float32y[i]
  end
  println("back Float32: $(S)")
  return S
end
backFloat32()

function backFloat64()
  S = Float64(0.0)
  for i = 5  : -1 : 1
      S = S + Float64x[i] * Float64y[i]
  end
    println("back Float64: $(S)")
  return S
end
backFloat64()

#(c)
function orderedDescFloat32()
  SNegative = Float32(0.0)
  SPositive = Float32(0.0)
  S = Float32(0.0)

  negativeArray = Float32[]
  positiveArray = Float32[]

  for i = 1 : 5
    element = Float32x[i] * Float32y[i]
    if(element > 0)
      push!(positiveArray, element) #adding the next element to the end of an array (positive values)
    else
      push!(negativeArray, element) #adding the next element to the end of an array (negative values)
    end
  end

  reverse!(sort!(positiveArray)) # sort array and next reverse it (positive value)
  sort!(negativeArray) # sort array (negative values)

  for i = 1 : length(negativeArray) #add negative numbers
    SNegative = SNegative + negativeArray[i]
  end

  for i = 1 : length(positiveArray) #add positive numbers
    SPositive = SPositive + positiveArray[i]
  end

  S = SPositive + SNegative
  println("ordered Desc Float32(): $(S)")
  return S
end
orderedDescFloat32()

function orderedDescFloat64()
  SNegative = Float64(0.0)
  SPositive = Float64(0.0)
  S = Float64(0.0)

  negativeArray = Float64[]
  positiveArray = Float64[]

  for i = 1 : 5
    element = Float64x[i] * Float64y[i];
    if(element > 0)
      push!(positiveArray, element)
    else
      push!(negativeArray, element)
    end
  end

  reverse!(sort!(positiveArray))
  sort!(negativeArray)

  for i = 1: length(negativeArray)
    SNegative = SNegative + negativeArray[i]
  end

  for i = 1: length(positiveArray)
    SPositive = SPositive + positiveArray[i]
  end

  S = SPositive + SNegative
  println("ordered Desc Float64(): $(S)")
  return S
end
orderedDescFloat64()

#(d) dodawanie rosnaco
function orderedAscFloat32()
  SNegative = Float32(0.0)
  SPositive = Float32(0.0)
  S = Float32(0.0)

  negativeArray = Float32[]
  positiveArray = Float32[]

  for i = 1 : 5
    element = Float32x[i] * Float32y[i];
    if(element > 0)
      push!(positiveArray, element)
    else
      push!(negativeArray, element)
    end
  end

  sort!(positiveArray)
  reverse!(sort!(negativeArray))

  for i = 1: length(negativeArray)
    SNegative = SNegative + negativeArray[i]
  end

  for i = 1: length(positiveArray)
    SPositive = SPositive + positiveArray[i]
  end

  S = SPositive + SNegative
  println("ordered Asc 32: $(S)")
  return S
end
orderedAscFloat32()

function orderedAscFloat64()
  SNegative = Float64(0.0)
  SPositive = Float64(0.0)
  S = Float64(0.0)

  negativeArray = Float64[]
  positiveArray = Float64[]

  for i = 1 : 5
    element = Float64x[i] * Float64y[i]
    if(element > 0)
      push!(positiveArray, element)
    else
      push!(negativeArray, element)
    end
  end

  sort!(positiveArray)
  reverse!(sort!(negativeArray))

  for i = 1: length(negativeArray)
    S = S + negativeArray[i]
  end

  for i = 1: length(positiveArray)
    S = S + positiveArray[i]
  end

  S = SPositive + SNegative
  println("ordered Asc 64: $(S)")
  return S
end
orderedAscFloat64()
