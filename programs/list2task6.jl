#Aleksandra Kuriata

c = [-2, -2, -2, -1, -1, -1, -1];
x = [1, 2, 1.99999999999999, 1, -1, 0.75, 0.25]

function RecursiveEquation(x, c)
  for i=1:40
    x = x * x + c
    println("iteration = $(i)     result = $(x)")
  end
   return x
end

function Zad5()
  for i = 1:length(c)
    currentC = c[i]
    currentX = x[i]
    println("c = $(currentC), x = $(currentX)")
    result = RecursiveEquation(currentX, currentC)
    println("result after 40 iteration = $(result)")
    println()
  end
end

Zad5()
