#Aleksandra Kuriata

p0 = 0.01f0
r = 3

#1
function recursiveEquation32(p::Float32, r, n)
  for i=1:n
    p = p + (r * p) * (1 - p)
  end
  return p
end

function cut()
 return parse(Float32,chop(@sprintf("%0.4f", (recursiveEquation32(p0, r, 10)))))
end

function task1()
  result40Iterations = recursiveEquation32(p0, r,40)

  result10Iterations = cut()
  println("Result 10 iterations: $(result10Iterations)")
  result30Iterations = recursiveEquation32(result10Iterations, r, 30)

  println("Result 40 iterations in arithmetic Float32: $(result40Iterations) ,   result 40 iterations with truncate(10 + 30): $(result30Iterations)")
  println()
end

#2
function recursiveEquation64(p, r, n)
  for i=1:n
    p = p + (r * p) * (1 - p)
  end
  return p
end

function task2()
  p0 = 0.01
  r = 3
  result40Iterations = recursiveEquation64(p0, r,40)
  println("Result 40 iterations in arithmetic Float64: $(result40Iterations)")
end

task1()
task2()
