#Aleksandra Kuriata


function hilb(n::Int)
# Function generates the Hilbert matrix  A of size n,
#  A (i, j) = 1 / (i + j - 1)
# Inputs:
#	n: size of matrix A, n>0
#
#
# Usage: hilb (10)
#
# Pawel Zielinski
        if n < 1
         error("size n should be > 0")
        end
        A= Array{Float64}(n,n)
        for j=1:n, i=1:n
                A[i,j]= 1 / (i + j - 1)
        end
        return A
end

function multiplicate(A, x)
        return A*x
end

function gauss(A, b)
        return A\b
end

function inverse(A, b)
        return inv(A)*b
end

#a
function task3a()
        println("Hn is Hilbert matrix")
        println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        for n = 1:20
                A = hilb(n)
                x = fill(1.0, n) #set the vector elements x o and elements on 1.0
                b = multiplicate(A, x)

                firstAlgorithm = gauss(A, b)
                secondAlgorithm = inverse(A, b)
                println("Range matrix: $(n)")
                println("Gaussian elimination: $(firstAlgorithm)")
                println("Inverted matrix: $(secondAlgorithm)")

                relativeErrorGaussianElimination = norm(firstAlgorithm - x) / norm(firstAlgorithm)
                println("Relative error gaussian elimination: $(relativeErrorGaussianElimination)")

                relativeErrorInvertedMatrix = norm(secondAlgorithm - x) / norm(secondAlgorithm)
                println("Relative error inverted matrix: $(relativeErrorInvertedMatrix)")

                println("------------------------------------------------------------------------------------")
                println(" ")
        end
        println(" ")
        println(" ")
end

#b
function matcond(n::Int, c::Float64)
# Function generates a random square matrix A of size n with
# a given condition number c.
# Inputs:
#	n: size of matrix A, n>1
#	c: condition of matrix A, c>= 1.0
#
# Usage: matcond (10, 100.0);
#
# Pawel Zielinski
        if n < 2
         error("size n should be > 1")
        end
        if c< 1.0
         error("condition number  c of a matrix  should be >= 1.0")
        end
        (U,S,V)=svd(rand(n,n))
        return U*diagm(linspace(1.0,c,n))*V'
end


function task3b()
        println("Rn is range matrix")
        println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        for n in [5, 10, 15]
                for c in [10.0^0, 10.0^1, 10.0^3, 10.0^7, 10.0^12, 10.0^16]
                        A = matcond(n, c)
                        x = fill(1.0, n) #set the vector elements x o and elements on
                        b = multiplicate(A, x)

                        firstAlgorithm = gauss(A, b)
                        secondAlgorithm = inverse(A, b)
                        println("Range matrix: $(n)")
                        println("Gaussian elimination: $(firstAlgorithm)")
                        println("Inverted matrix: $(secondAlgorithm)")

                        relativeErrorGaussianElimination = norm(firstAlgorithm - x) / norm(firstAlgorithm)
                        println("Relative error gaussian elimination: $(relativeErrorGaussianElimination)")

                        relativeErrorInvertedMatrix = norm(secondAlgorithm - x) / norm(secondAlgorithm)
                        println("Relative error inverted matrix: $(relativeErrorInvertedMatrix)")

                        println("------------------------------------------------------------------------------------")
                        println(" ")
                end
        end
end

task3a()
task3b()
